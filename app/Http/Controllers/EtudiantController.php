<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Etudiant;

class EtudiantController extends Controller
{
    //
    public function liste_etudiant()
    {   
        $etudiants = Etudiant::paginate(4);
        return view('etudiant.liste', compact('etudiants'));
    }

    public function ajouter_etudiant()
    {
        return view('etudiant.ajouter');
    }

    public function ajouter_etudiant_traitement(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'classe' => 'required',
        ]);
        
        $etudiant = new Etudiant(); 
        $etudiant->nom = $request->nom;
        $etudiant->prenom = $request->prenom;
        $etudiant->classe = $request->classe;
        $etudiant->save();

        return redirect()->route('/ajouter')-> with('success', 'Etudiant ajouté avec succès!'); //message de redirection avec succès
    }

    public function modifier_etudiant($id)
    {
        $etudiants = Etudiant::find($id);
        return view('etudiant.modifier', compact('etudiants'));
    }

    public function modifier_etudiant_traitement(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'classe' => 'required',
        ]);
        
        $etudiants = Etudiant::find($request->id); 
        $etudiants->nom = $request->nom;
        $etudiants->prenom = $request->prenom;
        $etudiants->classe = $request->classe;
        $etudiants->update();
        return redirect()->route('/etudiant')-> with('success', 'Etudiant modifié avec succès!'); //message de redirection avec succès
    }

    public function supprimer_etudiant($id)
    {
        $etudiants = Etudiant::find($id);
        $etudiants->delete();
        return redirect()->route('/etudiant')-> with('success', 'Etudiant supprimé avec succès!'); //message de redirection avec succès
    }
}
