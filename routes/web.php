<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EtudiantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// Route::get('/', function () {
//     return view('/etudiant/ajoute');
// });

Route::get('/modifier-etudiant/{id}', [EtudiantController::class, 'modifier_etudiant']);// affichage de la page de modification d'un étudiant
Route::get('/supprimer-etudiant/{id}', [EtudiantController::class, 'supprimer_etudiant']);
Route::post('/modifier/traitement', [EtudiantController::class, 'modifier_etudiant_traitement']);
Route::get ('/etudiant', [EtudiantController::class, 'liste_etudiant'])->name('/etudiant');// affichage de la page d'accueil avec une liste des étudiant
Route::get ('/ajouter', [EtudiantController::class, 'ajouter_etudiant'])->name('/ajouter');// affichage de la page d'ajout d'un étudiant
Route::post('/ajouter/traitement', [EtudiantController::class, 'ajouter_etudiant_traitement']); 




